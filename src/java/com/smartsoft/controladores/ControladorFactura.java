/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartsoft.controladores;

import com.smartsoft.datos.ClienteBD;
import com.smartsoft.datos.FacturaBD;
import com.smartsoft.datos.ProductoBD;
import com.smartsoft.logica.Detalle;
import com.smartsoft.logica.Factura;
import com.smartsoft.util.Restriction;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControladorFactura extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        
        String requestURI = request.getRequestURI();
        String url = "";
        
        if (requestURI.endsWith("/factura")) {
            request.setAttribute("lstCliente",  ClienteBD.select());
            request.setAttribute("lstProducto", ProductoBD.select());
            url = "/app/factura.jsp";
        }
        
        getServletContext()
            .getRequestDispatcher(url)
            .forward(request, response);        
    }
    
    @Override
    public void doPost(HttpServletRequest request, 
            HttpServletResponse response) 
            throws ServletException, IOException {
        
        String requestURI = request.getRequestURI();
        String url = "";
        
        if (requestURI.endsWith("/registraFactura")) {
            url = registrarFactura(request,response);
        }
        
        getServletContext()
            .getRequestDispatcher(url)
            .forward(request, response);        
    }
    
    // fmt: idProducto,nombre,precio,cantidad,subtotal
    public ArrayList<Detalle> strToDetalle(String s) {
        
        ArrayList<Detalle> lst = new ArrayList<Detalle>();
        String items[] = s.split(";");        
        
        for (int i = 0; i < items.length; i++) {            
            String cell[] = items[i].split(",");
            Detalle dt = new Detalle();
            dt.setIdProducto(Restriction.getNumber(cell[0]));
            dt.setCantidad(Restriction.getNumber(cell[3]));
            dt.setSubtotal(Restriction.getNumber(cell[4]));
            if (dt.getIdProducto() <= 0 || 
                dt.getCantidad() <= 0 || 
                dt.getSubtotal() <= 0) {
                return null;
            }
            lst.add(dt);
        }
        return lst;
    }
    
    public String registrarFactura(HttpServletRequest request,
                        HttpServletResponse response) {
        
        String cliente = request.getParameter("cliente");
        String detalle = request.getParameter("detalle");
        
        ArrayList<Detalle> ldetalle = strToDetalle(detalle);
        
        if (ldetalle != null && ldetalle.size() > 0) {
            
            Factura factura = new Factura();
            factura.setIdCliente(Restriction.getNumber(cliente));
            factura.setFecha(new Date());        
            factura.setDetalle(ldetalle);
            
            if (FacturaBD.insert(factura)) {
                request.setAttribute("msg", "* Factura almacenada correctamente.");
                request.setAttribute("msgType", "form-msg bottom-space alert alert-success");
            } else {
                request.setAttribute("msg", "* Factura no almacenada.");
                request.setAttribute("msgType", "form-msg bottom-space alert alert-warning");
            }            
        } else {
            request.setAttribute("msg", "* Factura no almacenada. Especifique correctamente los datos.");
            request.setAttribute("msgType", "form-msg bottom-space alert alert-info");
        }
        
        request.setAttribute("lstCliente", ClienteBD.select());
        request.setAttribute("lstProducto", ProductoBD.select());
        
        return "/app/factura.jsp";
    }
    
}
