/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartsoft.logica;

public class Producto {
    
    private int idProducto;
    private String nombre;
    private int precio;
    private int enExistencia;

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getEnExistencia() {
        return enExistencia;
    }

    public void setEnExistencia(int enExistencia) {
        this.enExistencia = enExistencia;
    }   
}
