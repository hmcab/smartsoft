/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartsoft.datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class PilaConexionesLocal {

    private static PilaConexionesLocal pila_con = null;
    private DataSource dataSource = null;    
    
    public synchronized static PilaConexionesLocal obtenerInstancia () {
        if (pila_con == null)
            pila_con = new PilaConexionesLocal();
        return pila_con;
    }
    
    public PilaConexionesLocal () {
       try {           
           InitialContext initContext = new InitialContext();
           dataSource = (DataSource) initContext.lookup("java:/comp/env/jdbc/rdw");
       } catch (NamingException e) {
           System.err.println(e);
       }
    }
    
    public Connection obtenerConexion () {        
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            System.err.println (e);
            return null;
        }
    }   
    
    public void liberarConexion (Connection con) {
        if (con != null) {
            try {
                con.setAutoCommit(true);
                con.close();
            } catch (SQLException e) {
                System.err.println (e);
            }
        }
    }
}