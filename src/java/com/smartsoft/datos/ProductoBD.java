/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartsoft.datos;

import com.smartsoft.logica.Producto;
import com.smartsoft.util.UtilBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductoBD {
    
    public static ArrayList<Producto> select() {
        
        PilaConexionesLocal pila_con = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String sql = "SELECT * FROM tbl_producto";
        
        try {
            pila_con = PilaConexionesLocal.obtenerInstancia();
            con = pila_con.obtenerConexion();
            ps  = con.prepareStatement(sql);
            rs  = ps.executeQuery();
            
            ArrayList<Producto> lst = new ArrayList<Producto>();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setIdProducto(rs.getInt("PK_PRODUCTO"));
                producto.setNombre(rs.getString("NOMBRE"));
                producto.setPrecio(rs.getInt("PRECIO"));
                producto.setEnExistencia(rs.getInt("EXISTENCIA"));
                
                if (producto.getEnExistencia() > 0) {
                    lst.add(producto);  
                }
            }
            return lst;
            
        } catch (SQLException | NullPointerException e) {
            System.err.println(e);
        } finally {
            UtilBD.closeResultSet(rs);
            UtilBD.closePreparedStatement(ps);
            if (pila_con != null)
                pila_con.liberarConexion(con);
        }
        return null;
    }
}
