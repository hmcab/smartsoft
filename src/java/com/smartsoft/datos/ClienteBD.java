/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartsoft.datos;

import com.smartsoft.logica.Cliente;
import com.smartsoft.util.UtilBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ClienteBD {
    
    public static ArrayList<Cliente> select() {
        
        PilaConexionesLocal pila_con = null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        String sql = "SELECT * FROM tbl_cliente";
        
        try {
            pila_con = PilaConexionesLocal.obtenerInstancia();
            con = pila_con.obtenerConexion();
            ps  = con.prepareStatement(sql);
            rs  = ps.executeQuery();
            
            ArrayList<Cliente> lst = new ArrayList<Cliente>();
            while (rs.next()) {
                Cliente cliente = new Cliente();
                cliente.setIdCliente(rs.getInt("PK_CLIENTE"));
                cliente.setNombre(rs.getString("NOMBRE"));
                cliente.setApellido(rs.getString("APELLIDO"));
                cliente.setFechaNacimiento(rs.getDate("FECHA_NACIMIENTO"));
                cliente.setTelefono(rs.getString("TELEFONO"));
                cliente.setDireccion(rs.getString("DIRECCION"));
                cliente.setEmail(rs.getString("EMAIL"));
                lst.add(cliente);
            }
            return lst;
            
        } catch (SQLException | NullPointerException e) {
            System.err.println(e);
        } finally {
            UtilBD.closeResultSet(rs);
            UtilBD.closePreparedStatement(ps);
            if (pila_con != null)
                pila_con.liberarConexion(con);
        }
        return null;
    }
    
}
