/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartsoft.datos;

import com.smartsoft.logica.Detalle;
import com.smartsoft.logica.Factura;
import com.smartsoft.util.UtilBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class FacturaBD {
    
    public static String sql_detalle(ArrayList<Detalle> lst) {
        
        String sql  = "INSERT INTO tbl_detalle (FK_FACTURA, FK_PRODUCTO, CANTIDAD, SUBTOTAL) VALUES ";
        String data = "";
        
        for (int i = 0; i < lst.size(); i++) {
            Detalle detalle = lst.get(i);
            String str = 
                    String.format("(%d,%d,%d,%d)",
                    detalle.getIdFactura(),
                    detalle.getIdProducto(),
                    detalle.getCantidad(),
                    detalle.getSubtotal());
            data += (data == "") ? str : "," + str;
        }
        return sql + data;
    }
    
    public static boolean insert(Factura factura) {
        
        PilaConexionesLocal pila_con = null;
        Connection con = null;
        PreparedStatement ps = null;        
        ResultSet rs = null;
        
        String sql_factura = "INSERT INTO tbl_factura (FK_CLIENTE,FECHA)"
                                + " VALUES (?,?)";        
        int n = 0;
        SimpleDateFormat ffmt = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            pila_con = PilaConexionesLocal.obtenerInstancia();
            con = pila_con.obtenerConexion();
            con.setAutoCommit(false);
            ps = con.prepareStatement(sql_factura, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, factura.getIdCliente());
            ps.setString(2, ffmt.format(factura.getFecha()));
            n  = ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            
            ArrayList<Detalle> ldetalle = factura.getDetalle();
            if (rs.next()) {
                int idFactura = rs.getInt(1);                
                for (Detalle detalle : ldetalle) {
                    detalle.setIdFactura(idFactura);
                }
                String sql_detalle = sql_detalle(ldetalle);
                ps = con.prepareStatement(sql_detalle);
                n += ps.executeUpdate();
            }
            
            if (n == ldetalle.size()+1) {
                con.commit();
                return true;
            } else {
                con.rollback();
            }
            
        } catch (SQLException | NullPointerException e) {
            System.err.println(e);
            try { if (con != null) { con.rollback(); } }
            catch (SQLException ie) { System.err.println(ie); }
        } finally {
            UtilBD.autoCommit(con);
            UtilBD.closePreparedStatement(ps);
            if (pila_con != null)
                pila_con.liberarConexion(con);
        }
        return false;
    }
    
}
