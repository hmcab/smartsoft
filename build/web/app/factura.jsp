<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/include/header.jsp" />

<div class="col-lg-9 centered">    
    <section class="boxed padding"> 
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#">Factura</a></li>            
        </ul>
        <div class="tab-content centered">
            <div role="tabpanel" class="tab-pane padding active">                    

                <form role="form" id="form-registra-factura" class="form-horizontal" 
                      action="<c:url value='/registraFactura' />" method="post">
                    
                    <div id="msg" class="form-msg bottom-space ${msgType}" role="alert">${msg}</div>
                    
                    <div class="row">
                        <div class="control-group">
                            <label class="col-sm-2">* Cliente</label>
                            <div class="col-sm-6">
                                <select id="cliente" name="cliente" class="form-control" title="Seleccione un cliente">
                                    <option value="">Seleccione un cliente</option>
                                    <c:forEach items="${lstCliente}" var="cliente">
                                        <option value="${cliente.idCliente}">${cliente.nombre} ${cliente.apellido}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="control-group">
                            <label class="col-sm-2">* Producto</label>
                            <div class="col-sm-6">
                                <select id="producto" name="producto" class="form-control" title="Seleccione un producto">
                                    <option value="">Seleccione un producto</option>
                                    <c:forEach items="${lstProducto}" var="producto">
                                        <option value="${producto.idProducto};${producto.nombre};${producto.enExistencia};${producto.precio}">${producto.nombre}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row top-xl-space">
                        <div class="control-group">
                            <label class="col-sm-2">En existencia</label>
                            <label class="col-sm-2">Precio</label>
                            <label class="col-sm-2">Cantidad</label>
                            <label class="col-sm-2">Subtotal</label>
                            <label class="col-sm-2">Agregar / Quitar</label>                            
                        </div>
                    </div>
                    
                    <div class="row bottom-space">
                        <div class="control-group">
                            <div class="col-sm-2"><input type="text" id="existencia" name="existencia" readonly="readonly" /></div>
                            <div class="col-sm-2"><input type="text" id="precio" name="precio" readonly="readonly" /></div>
                            <div class="col-sm-2"><input type="number" id="cantidad" name="cantidad" min="0" /></div>
                            <div class="col-sm-2"><input type="text" id="subtotal" name="subtotal" readonly="readonly" /></div>
                            <div class="col-sm-2">
                                <input type="button" onclick="ft_agregaProducto();" value="Agregar" style="width:60px;" />
                                <input type="button" onclick="ft_eliminaProducto();" value="Quitar" style="width:60px;" />
                            </div>
                            <input type="hidden" id="idProducto" name="idProducto" />
                            <input type="hidden" id="nomProducto" name="nomProducto" />
                        </div>
                    </div>                                        
                    
                    <div class="control-group col-sm-9 top-xl-space">
                        <table id="tblDetalle" name="tblDetalle">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Producto</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                        </table>
                    </div>             
                    <input type="hidden" id="detalle" name="detalle" />
                    
                    <div class="form-group">                                                                                    
                        <div class="col-sm-offset-2 col-sm-7" style="padding-top: 10px; margin-left: 15px;">
                            <button style="display:none;" type="submit" class="btn"></button>
                            <button style="width:126px;" type="button" class="btn" title="Haga click para registrar" 
                                    onclick="ft_registraFactura();">Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>        
    </section>
</div>

<jsp:include page="/include/footer.jsp" />
<script>
    $(document).ready(function() {         
        $('#tblDetalle').DataTable({            
            responsive: true,            
            scrollY: 300,
            searching: true,
            bAutoWidth: true,
            bInfo: false,
            paging: false,
            language: {url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"}
        });
        $('#producto').on("change", ft_cargaProducto);
        $('#cantidad').on("click",  ft_calculaSubtotal);
        $('#cantidad').on("blur",   ft_calculaSubtotal);
        $('#cantidad').on("change", ft_calculaSubtotal);
    });
</script>
<jsp:include page="/include/end.jsp" />

