<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
    <head>
        <title>SmartSoft</title>        
        <!-- Metatags -->
        <meta charset="utf-8">   
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- css -->                
        <link rel="stylesheet"  href="/Smartsoft/htdocs/externo/DataTables/media/css/jquery.dataTables.min.css" />
        <link rel="stylesheet"  href="/Smartsoft/htdocs/externo/DataTables/media-responsive/css/responsive.dataTables.css" />
        <link rel="stylesheet"  href="/Smartsoft/htdocs/externo/DataTables/media/css/rowReorder.dataTables.min.css" />
        <link rel="stylesheet"  href="/Smartsoft/htdocs/externo/bootstrap-3.3.7/css/bootstrap.css" />
        <link rel="stylesheet"  href="/Smartsoft/htdocs/externo/bootstrap-3.3.7/css/bootstrap-datepicker.css" />
        <link rel="stylesheet"  href="/Smartsoft/htdocs/externo/bootstrap-3.3.7/css/bootstrap-datepicker-1.6.4.css" />

        <link rel="stylesheet"  href="/Smartsoft/htdocs/css/style.css" />      
    </head>
    <body>