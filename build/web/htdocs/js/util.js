
function ft_calculaSubtotal() {
    var idPdo    = document.getElementById("idProducto");
    var esNumero = exp_numerico;
        
    if (idPdo !== null && idPdo.value !== "" && esNumero.test(idPdo.value)) {
        
        var existencia = document.getElementById("existencia");
        var cantidad   = document.getElementById("cantidad");
        var precio     = document.getElementById("precio");
        var subtotal   = document.getElementById("subtotal");        
        
        if (esNumero.test(existencia.value) &&
            esNumero.test(cantidad.value) &&
            esNumero.test(precio.value)) {
            var nexistencia = parseInt(existencia.value);
            var ncantidad   = parseInt(cantidad.value);
            var nprecio     = parseInt(precio.value);
            if (ncantidad > nexistencia) {
                cantidad.value = nexistencia;
                subtotal.value = nprecio * nexistencia;
            } else {
                subtotal.value = nprecio * ncantidad;
            }
        }
    }
}

function ft_cargaProducto() {
    var pdo = document.getElementById("producto");    
    
    if (pdo !== null && pdo.value !== "") {        
        var arrayPdo = pdo.value.split(";");
        var id         = arrayPdo[0];
        var nombre     = arrayPdo[1];
        var existencia = arrayPdo[2];
        var precio     = arrayPdo[3];
        document.getElementById("idProducto").value = id;
        document.getElementById("nomProducto").value = nombre;
        document.getElementById("existencia").value = existencia;
        document.getElementById("precio").value = precio;
        document.getElementById("cantidad").value = 1;
        ft_calculaSubtotal();
    } else {
        document.getElementById("idProducto").value = "";
        document.getElementById("nomProducto").value = "";
        document.getElementById("existencia").value = "";
        document.getElementById("precio").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("subtotal").value = "";
    }
}

function ft_agregaProducto() {    
    
    var tbl      = $('#tblDetalle').DataTable();
    var idPdo    = document.getElementById("idProducto");
    var nomPdo   = document.getElementById("nomProducto").value;
    var precio   = document.getElementById("precio").value;
    var cantidad = document.getElementById("cantidad").value;
    var subtotal = document.getElementById("subtotal").value;    
        
    if (tbl !== null && idPdo !== null && idPdo.value !== "") {        
        var id = idPdo.value;
        // var index = tbl.column(0).data().indexOf(id);
        var indexes = [];
        tbl.rows(function(idx,data,node) {
            if (data[0] === id) {
                indexes.push(idx);
            }            
        });
        var index = (indexes.length > 0) ? indexes[0] : -1;
        if (index < 0) {
            //console.log(id + " No existe");
            tbl.row.add([
                id, nomPdo, precio, cantidad, subtotal
            ]).draw(true);
        } else {
            //console.log(id + " encontrado en index " + index);
            tbl.row(index).remove().draw(true);            
            tbl.row.add([
                id, nomPdo, precio, cantidad, subtotal
            ]).draw(true);
        }
    }
}

function ft_eliminaProducto() {
    
    var tbl   = $('#tblDetalle').DataTable();
    var idPdo = document.getElementById("idProducto");
    
    if (tbl !== null && idPdo !== null && idPdo.value !== "") {
        var id = idPdo.value;
        
        var indexes = [];
        tbl.rows(function(idx,data,node) {
            if (data[0] === id) {
                indexes.push(idx);
            }            
        });
        var index = (indexes.length > 0) ? indexes[0] : -1;
        if (index >= 0) {
            tbl.row(index).remove().draw(true);
        }
    }
}

// fmt: idProducto,nombre,precio,cantidad,subtotal
function ft_registraFactura() {
    
    var cliente = document.getElementById("cliente");
    var msg     = document.getElementById("msg");
    var tbl     = $('#tblDetalle').DataTable();
    var data    = tbl.rows().data();
    var strdata = "";
    var vmsg    = "";
    
    for (var i = 0; i < data.length; i++) {
        var n = data[i].length;
        var str = "";
        for (var j = 0; j < n; j++) {
            var campo = data[i][j];
            str += (str === "") ? campo : "," + campo;
        }
        strdata += (strdata === "") ? str : ";" + str;
    }
    
    if (cliente === null || cliente.value === "") {
        vmsg = "* Especifique un cliente.";        
    } else if (strdata === "") {
        vmsg = "* Especifique al menos un producto a facturar.";
    }
    
    if (vmsg !== "") {
        msg.innerHTML = vmsg;
        msg.setAttribute("class", "form-msg bottom-space alert alert-info");
    } else {
        var form = "#form-registra-factura";
        document.getElementById("detalle").value = strdata;
        // Accionar submit salta las restricciones de los campos
        // document.getElementById(form).submit();    
        $(form).find('[type="submit"]').trigger('click');            
    }
}

